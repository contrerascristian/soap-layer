<?php
/**
 * BaseObject for webpay basic implementation
 *
 * @author Cristian Contreras <ccontrerasl@gmail.com>
 * @version 1.0.0
 */
namespace Tiandgi\SoapLayer;

/**
 * Base Object
 */
class BaseObject{

    /**
     * Constructor
     * @since Version 1.0
     * @param Array $attributes
     */
    public function __construct($attributes = Array()){
      foreach($attributes as $field=>$value){
        $this->$field = $value;
      }
    }

    /**
     * Override Setter
     * @param $name
     * @param $value
     */
    function __set($name,$value){
      if(method_exists($this, $name)){
        $this->$name($value);
      }
      else{
        $this->$name = $value;
      }
    }

    /**
     * Override Getter
     * @param  $name
     * @return mixed
     */
    function __get($name){
      if(method_exists($this, $name)){
        return $this->$name();
      }
      elseif(property_exists($this,$name)){
        return $this->$name;
      }
      return null;
    }

    /**
     * Return object in JSON format
     * @return String
     */
    function toJSON(){
      return json_encode($this);
    }

    /**
     * Override toString returning object dump
     * @return string
     */
    function __toString()
     {
        $vars="Variables:";
        foreach(get_class_vars(__CLASS__) as $name => $value) $vars.="<br> $name : {$this->{$name}}".gettype($this->{$name});
        return '<pre>'.__CLASS__.':<br>'.$vars.'<br>Methods:<br>'.implode('<br>',get_class_methods(__CLASS__)).'</pre>';
     }
}

?>
