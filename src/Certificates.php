<?php

namespace Tiandgi\SoapLayer\Certificate;
use Tiandgi\SoapLayer;
class Certificates extends \Tiandgi\SoapLayer\BaseObject{

    public $client_private_key;
    public $client_certificate;
    public $server_certificate;


    public function __construct($client_private_key, $client_certificate, $server_certificate = null)
    {
        $this->client_private_key = $client_private_key;
        $this->client_certificate = $client_certificate;
        $this->server_certificate = $server_certificate === null ? dirname(__FILE__) . '/cert/tbk.pem' : $server_certificate;
    }


}
