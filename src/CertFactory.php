<?php
	namespace Tiandgi\SoapLayer\Certificate;

	class CertFactory{
		public static function IntegracionWPN()
		{
			$private_key = dirname(__FILE__) . '/cert/normal_clp/597020000541.key';
			$client_certificate = dirname(__FILE__) . '/cert/normal_clp/597020000541.crt';
			$tbk_certificate = dirname(__FILE__) . '/cert/normal_clp/tbk.pem';
			return self::Create($private_key, $client_certificate, $tbk_certificate);
		}

		public static function IntegracionPP()
		{
			$private_key = dirname(__FILE__) . '/cert/patpass/597020000548.key';
			$client_certificate = dirname(__FILE__) . '/cert/patpass/597020000548.crt';
			return self::Create($private_key, $client_certificate);
		}

		public static function production($client_private_key, $client_certificate, $server_certificate = null)
		{
			return self::Create($client_private_key, $client_certificate, $server_certificate);
		}

		private static function create($client_private_key, $client_certificate, $server_certificate = null)
		  {
			return new Certificates($client_private_key, $client_certificate, $server_certificate);
		}
	}
 ?>
