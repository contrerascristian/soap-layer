<?php
/**
 * base webpay configuration object
 *
 * @author Cristian Contreras <ccontrerasl@gmail.com>
 * @version 1.0.0
 */

namespace Tiandgi\SoapLayer;

class Configuration extends BaseObject{

    public $commerce_code;
    public $private_key;
    public $public_cert;
    public $webpay_cert;
    public $store_codes;

    private static $resultCodes = array(
             "0" => "Transacción aprobada",
            "-1" => "Rechazo de transacción",
            "-2" => "Transacción debe reintentarse",
            "-3" => "Error en transacción",
            "-4" => "Rechazo de transacción",
            "-5" => "Rechazo por error de tasa",
            "-6" => "Excede cupo máximo mensual",
            "-7" => "Excede límite diario por transacción",
            "-8" => "Rubro no autorizado",
    );

   final public static function ResultCodeMessage($resultCode)
   {
        return self::$resultCodes[$resultCode];
   }


   final public static function soapOptions(){
       return array(
            "classmap" => '',
            "trace" => true,
            "exceptions" => true
       );
   }

   final public static function WPNEndpoints(){
       return array(
           "INTEGRACION"   => "https://webpay3gint.transbank.cl/WSWebpayTransaction/cxf/WSWebpayService?wsdl",
           "CERTIFICACION" => "https://webpay3gint.transbank.cl/WSWebpayTransaction/cxf/WSWebpayService?wsdl",
           "PRODUCCION"    => "https://webpay3g.transbank.cl/WSWebpayTransaction/cxf/WSWebpayService?wsdl"
       );
   }

   final public static function NullifyEndpoints(){
       return array(
           "INTEGRACION"   => "https://webpay3gint.transbank.cl/WSWebpayTransaction/cxf/WSCommerceIntegrationService?wsdl",
           "CERTIFICACION" => "https://webpay3gint.transbank.cl/WSWebpayTransaction/cxf/WSCommerceIntegrationService?wsdl",
           "PRODUCCION"    => "https://webpay3g.transbank.cl/WSWebpayTransaction/cxf/WSCommerceIntegrationService?wsdl"
       );
   }

   final public static function WPNClassMap()
   {
       return array (
             'acknowledgeTransaction'          => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructAcknowledgeTransaction::class,
             'acknowledgeTransactionResponse'  => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructAcknowledgeTransactionResponse::class,
             'cardDetail'                      => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructCardDetail::class,
             'getTransactionResult'            => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructGetTransactionResult::class,
             'getTransactionResultResponse'    => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructGetTransactionResultResponse::class,
             'initTransaction'                 => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructInitTransaction::class,
             'initTransactionResponse'         => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructInitTransactionResponse::class,
             'transactionResultOutput'         => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructTransactionResultOutput::class,
             'wpmDetailInput'                  => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructWpmDetailInput::class,
             'wsInitTransactionInput'          => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructWsInitTransactionInput::class,
             'wsInitTransactionOutput'         => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructWsInitTransactionOutput::class,
             'wsTransactionDetail'             => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructWsTransactionDetail::class,
             'wsTransactionDetailOutput'       => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPStructWsTransactionDetailOutput::class,
             'wsTransactionType'               => \Tiandgi\SoapLayer\TransbankNormalTransaction\WPEnumWsTransactionType::class
       );
   }

   final public static function NullifyClassMap(){
       return array(
           'baseBean'                        => 'Nullify_StructBaseBean',
           'capture'                         => 'Nullify_StructCapture',
           'captureInput'                    => 'Nullify_StructCaptureInput',
           'captureOutput'                   => 'Nullify_StructCaptureOutput',
           'captureResponse'                 => 'Nullify_StructCaptureResponse',
           'nullificationInput'              => 'Nullify_StructNullificationInput',
           'nullificationOutput'             => 'Nullify_StructNullificationOutput',
           'nullify'                         => 'Nullify_StructNullify',
           'nullifyResponse'                 => 'Nullify_StructNullifyResponse',
       );
   }
}

?>
