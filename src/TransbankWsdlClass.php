<?php
namespace Tiandgi\SoapLayer;

use \Tiandgi\SoapLayer;
use \Tiandgi\SoapLayer\External;
use \Tiandgi\SoapLayer\Certificate;
use \Tiandgi\SoapLayer\TransbankNormalTransaction;

class TransbankWsdlClass extends \stdClass implements \ArrayAccess,\Iterator,\Countable
{
    private static $soapClient;
    public $result;
    public $lastError;
    public $service = "WPN";
    public $environment = "INTEGRACION";
    public $config;


    public function __construct($_arrayOfValues = array(),$_resetSoapClient = false)
    {
        $this->setLastError(array());
        $this->initInternArrayToIterate($_arrayOfValues);
        if(is_array($_arrayOfValues) && count($_arrayOfValues))
        {
            foreach($_arrayOfValues as $name=>$value)
                $this->$name=$value;
        }
        if($_resetSoapClient)
            $this->initSoapClient();
    }


    private static function setSoapClient($_soapClient)
    {
        return (self::$soapClient = $_soapClient);
    }

    protected function getSoapClient(){
        return self::$soapClient;

    }
    public function initSoapClient()
    {
        $wsdlOptions = Configuration::soapOptions();
        $wsdlOptions['classmap'] = call_user_func(array("\Tiandgi\SoapLayer\Configuration", $this->service.'ClassMap'));
        $wsdl_url = call_user_func(array("\Tiandgi\SoapLayer\Configuration", $this->service.'Endpoints'))[$this->environment];
        self::setSoapClient(new \Tiandgi\SoapLayer\Transbank_SoapClient($wsdl_url, $this->config->private_key, $this->config->public_cert, $this->config->webpay_cert, $wsdlOptions));
   }



    public function setLocation($_location)
    {
        return self::getSoapClient()?self::getSoapClient()->__setLocation($_location):false;
    }

    public function getLastRequest($_asDomDocument = false)
    {
        if(self::getSoapClient())
            return self::getFormatedXml(self::getSoapClient()->__getLastRequest(),$_asDomDocument);
        return null;
    }

    public function getLastResponse($_asDomDocument = false)
    {
        if(self::getSoapClient() && $this->validateResponse(self::getFormatedXml(self::getSoapClient()->__getLastResponse(),$_asDomDocument)))
            return self::getFormatedXml(self::getSoapClient()->__getLastResponse(),$_asDomDocument);
        return null;
    }



    public function getLastRequestHeaders($_asArray = false)
    {
        $headers = self::getSoapClient()?self::getSoapClient()->__getLastRequestHeaders():null;
        if(is_string($headers) && $_asArray)
            return self::convertStringHeadersToArray($headers);
        return $headers;
    }

    public function getLastResponseHeaders($_asArray = false)
    {
        $headers = self::getSoapClient()?self::getSoapClient()->__getLastResponseHeaders():null;
        if(is_string($headers) && $_asArray)
            return self::convertStringHeadersToArray($headers);
        return $headers;
    }

    public static function getFormatedXml($_string,$_asDomDocument = false)
    {
        if(!empty($_string) && class_exists('DOMDocument'))
        {
            $dom = new DOMDocument('1.0','UTF-8');
            $dom->formatOutput = true;
            $dom->preserveWhiteSpace = false;
            $dom->resolveExternals = false;
            $dom->substituteEntities = false;
            $dom->validateOnParse = false;
            if($dom->loadXML($_string))
                return $_asDomDocument?$dom:$dom->saveXML();
        }
        return $_asDomDocument?null:$_string;
    }

    public static function convertStringHeadersToArray($_headers)
    {
        $lines = explode("\r\n",$_headers);
        $headers = array();
        foreach($lines as $line)
        {
            if(strpos($line,':'))
            {
                $headerParts = explode(':',$line);
                $headers[$headerParts[0]] = trim(implode(':',array_slice($headerParts,1)));
            }
        }
        return $headers;
    }

    public function setSoapHeader($_nameSpace,$_name,$_data,$_mustUnderstand = false,$_actor = null)
    {
        if(self::getSoapClient())
        {
            $defaultHeaders = (isset(self::getSoapClient()->__default_headers) && is_array(self::getSoapClient()->__default_headers))?self::getSoapClient()->__default_headers:array();
            foreach($defaultHeaders as $index=>$soapheader)
            {
                if($soapheader->name == $_name)
                {
                    unset($defaultHeaders[$index]);
                    break;
                }
            }
            self::getSoapClient()->__setSoapheaders(null);
            if(!empty($_actor))
                array_push($defaultHeaders,new SoapHeader($_nameSpace,$_name,$_data,$_mustUnderstand,$_actor));
            else
                array_push($defaultHeaders,new SoapHeader($_nameSpace,$_name,$_data,$_mustUnderstand));
            return self::getSoapClient()->__setSoapheaders($defaultHeaders);
        }
        else
            return false;
    }

    public function setHttpHeader($_headerName,$_headerValue)
    {
        if(self::getSoapClient() && !empty($_headerName))
        {
            $streamContext = (isset(self::getSoapClient()->_stream_context) && is_resource(self::getSoapClient()->_stream_context))?self::getSoapClient()->_stream_context:null;
            if(!is_resource($streamContext))
            {
                $options = array();
                $options['http'] = array();
                $options['http']['header'] = '';
            }
            else
            {
                $options = stream_context_get_options($streamContext);
                if(is_array($options))
                {
                    if(!array_key_exists('http',$options) || !is_array($options['http']))
                    {
                        $options['http'] = array();
                        $options['http']['header'] = '';
                    }
                    elseif(!array_key_exists('header',$options['http']))
                        $options['http']['header'] = '';
                }
                else
                {
                    $options = array();
                    $options['http'] = array();
                    $options['http']['header'] = '';
                }
            }
            if(count($options) && array_key_exists('http',$options) && is_array($options['http']) && array_key_exists('header',$options['http']) && is_string($options['http']['header']))
            {
                $lines = explode("\r\n",$options['http']['header']);

                $newLines = array();
                foreach($lines as $line)
                {
                    if(!empty($line) && strpos($line,$_headerName) === false)
                        array_push($newLines,$line);
                }

                array_push($newLines,"$_headerName: $_headerValue");

                $options['http']['header'] = implode("\r\n",$newLines);

                if(!is_resource($streamContext))
                    return (self::getSoapClient()->_stream_context = stream_context_create($options))?true:false;

                else
                    return stream_context_set_option(self::getSoapClient()->_stream_context,'http','header',$options['http']['header']);
            }
            else
                return false;
        }
        else
            return false;
    }

    public function length()
    {
        return $this->count();
    }

    public function count()
    {
        return $this->getInternArrayToIterateIsArray()?count($this->getInternArrayToIterate()):-1;
    }

    public function current()
    {
        return $this->offsetGet($this->internArrayToIterateOffset);
    }

    public function next()
    {
        return $this->setInternArrayToIterateOffset($this->getInternArrayToIterateOffset() + 1);
    }

    public function rewind()
    {
        return $this->setInternArrayToIterateOffset(0);
    }

    public function valid()
    {
        return $this->offsetExists($this->getInternArrayToIterateOffset());
    }

    public function key()
    {
        return $this->getInternArrayToIterateOffset();
    }

    public function item($_index)
    {
        return $this->offsetGet($_index);
    }

    public function add($_item)
    {
        if($this->getAttributeName() != '' && stripos($this->__toString(),'array') !== false)
        {

            if(!is_array($this->_get($this->getAttributeName())))
                $this->_set($this->getAttributeName(),array());

            $currentArray = $this->_get($this->getAttributeName());
            array_push($currentArray,$_item);
            $this->_set($this->getAttributeName(),$currentArray);
            $this->setInternArrayToIterate($currentArray);
            $this->setInternArrayToIterateIsArray(true);
            $this->setInternArrayToIterateOffset(0);
            return true;
        }
        return false;
    }

    public function toSend()
    {
        if($this->getAttributeName() != '' && stripos($this->__toString(),'array') !== false)
            return $this->_get($this->getAttributeName());
        else
            return null;
    }

    public function first()
    {
        return $this->item(0);
    }

    public function last()
    {
        return $this->item($this->length() - 1);
    }

    public function offsetExists($_offset)
    {
        return ($this->getInternArrayToIterateIsArray() && array_key_exists($_offset,$this->getInternArrayToIterate()));
    }

    public function offsetGet($_offset)
    {
        return $this->offsetExists($_offset)?$this->internArrayToIterate[$_offset]:null;
    }

    public function offsetSet($_offset,$_value)
    {
        return null;
    }

    public function offsetUnset($_offset)
    {
        return null;
    }

    public function getResult()
    {
        return $this->result;
    }

    protected function setResult($_result)
    {
        return ($this->result = $_result);
    }

    public function getLastError()
    {
        return $this->lastError;
    }

    private function setLastError($_lastError)
    {
        return ($this->lastError = $_lastError);
    }

    protected function saveLastError($_methoName,SoapFault $_soapFault)
    {
        return ($this->lastError[$_methoName] = $_soapFault);
    }

    public function getLastErrorForMethod($_methoName)
    {
        return (is_array($this->lastError) && array_key_exists($_methoName,$this->lastError))?$this->lastError[$_methoName]:null;
    }

    public function getInternArrayToIterate()
    {
        return $this->internArrayToIterate;
    }

    public function setInternArrayToIterate($_internArrayToIterate)
    {
        return ($this->internArrayToIterate = $_internArrayToIterate);
    }

    public function getInternArrayToIterateOffset()
    {
        return $this->internArrayToIterateOffset;
    }

    public function initInternArrayToIterate($_array = array(),$_internCall = false)
    {
        if(stripos($this->__toString(),'array') !== false)
        {
            if(is_array($_array) && count($_array))
            {
                $this->setInternArrayToIterate($_array);
                $this->setInternArrayToIterateOffset(0);
                $this->setInternArrayToIterateIsArray(true);
            }
            elseif(!$_internCall && $this->getAttributeName() != '' && property_exists($this->__toString(),$this->getAttributeName()))
                $this->initInternArrayToIterate($this->_get($this->getAttributeName()),true);
        }
    }

    public function setInternArrayToIterateOffset($_internArrayToIterateOffset)
    {
        return ($this->internArrayToIterateOffset = $_internArrayToIterateOffset);
    }

    public function getInternArrayToIterateIsArray()
    {
        return $this->internArrayToIterateIsArray;
    }

    public function setInternArrayToIterateIsArray($_internArrayToIterateIsArray = false)
    {
        return ($this->internArrayToIterateIsArray = $_internArrayToIterateIsArray);
    }

    public function _set($_name,$_value)
    {
        $setMethod = 'set' . ucfirst($_name);
        if(method_exists($this,$setMethod))
        {
            $this->$setMethod($_value);
            return true;
        }
        else
            return false;
    }

    public function _get($_name)
    {
        $getMethod = 'get' . ucfirst($_name);
        if(method_exists($this,$getMethod))
            return $this->$getMethod();
        else
            return false;
    }

    public function getAttributeName()
    {
        return '';
    }

    public static function valueIsValid($_value)
    {
        return true;
    }

    public function __toString()
    {
        return __CLASS__;
    }
}
