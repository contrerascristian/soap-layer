<?php
namespace Tiandgi\SoapLayer\Log;

class LogHandler
{
	public static function log($string, $level = iLogger::LEVEL_INFO, $type = null)
	{
		LoggerFactory::logger()->log($string, $level, $type);
	}
}

?>
