<?php
namespace Tiandgi\SoapLayer\Log;

class LoggerFactory
{

	private static $instance;
	public static function setLogger(TbkLogger $instance)
	{
		self::$instance = $instance;
	}
	public static function logger()
	{
		if (!self::$instance)
			self::initDefaultLogger();
		return self::$instance;
	}
	public static function initDefaultLogger()
	{
		return self::$instance = new VoidLogger();
	}

	private function __construct() {}
	private function __clone() {}
	private function __sleep() {}
	private function __wakeup() {}
}
 ?>
