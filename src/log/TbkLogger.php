<?php

namespace Tiandgi\SoapLayer\Log;

class TbkLogger implements iLogger
{

    private $dir;

    public function __construct($dir = '')
    {
        $this->dir = rtrim($dir, '/');
        if (!is_dir($this->dir))
            mkdir($this->dir, 0775, true);
    }
    public function log($data, $level = self::LEVEL_INFO, $type = null)
    {
        if (!is_string($data)) $data = print_r($data, true);
        file_put_contents(
            $this->getLogDirectory() . '/' . $this->getLogFilname(),
            $this->getLogMessage($data, $level, $type),
            FILE_APPEND
        );
    }
    protected function getLogDirectory()
    {
        return $this->dir;
    }
    protected function getLogFilname()
    {
        return date('Ymd') . '.transbank.log';
    }
    protected function getLogMessage($data, $level, $type)
    {
        $time = date('d/m/Y H:i:s');
        return "\n=================================\n$time - " . strtoupper($level) . " ($type):\n=================================\n$data";
    }
}


 ?>
