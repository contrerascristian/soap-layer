<?php

namespace Tiandgi\SoapLayer\Log;

interface iLogger
{
	const LEVEL_ERROR = 'error';
	const LEVEL_WARNING = 'warning';
	const LEVEL_INFO = 'info';
	public function log($data, $level = self::LEVEL_INFO, $type = null);
}
 ?>
