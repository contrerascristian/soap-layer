<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructAcknowledgeTransaction extends Tiandgi\SoapLayer\TransbankWsdlClass
{

    public $tokenInput;

    public function __construct($_tokenInput = NULL)
    {
        parent::__construct(array('tokenInput'=>$_tokenInput),false);
    }

    public function getTokenInput()
    {
        return $this->tokenInput;
    }

    public function setTokenInput($_tokenInput)
    {
        return ($this->tokenInput = $_tokenInput);
    }

    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }

}
