<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPServiceAcknowledge extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public function acknowledgeTransaction(WPStructAcknowledgeTransaction $_wPStructAcknowledgeTransaction)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->acknowledgeTransaction($_wPStructAcknowledgeTransaction));
        }
        catch(Exception $e)
        {
            return !$this->saveLastError(__METHOD__,$e);
        }
    }

    public function getResult()
    {
        return parent::getResult();
    }

}
