<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructAcknowledgeTransactionResponse extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public function __toString()
    {
        return __CLASS__;
    }
}
