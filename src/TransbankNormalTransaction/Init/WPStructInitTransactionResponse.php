<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructInitTransactionResponse extends \Tiandgi\SoapLayer\BaseObject
{

    public $return;

    public function __toString()
    {
        return __CLASS__;
    }
}
