<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPServiceInit extends \Tiandgi\SoapLayer\TransbankWsdlClass
{

    public function initTransaction(WPStructInitTransaction $_wPStructInitTransaction)
    {
           return $this->setResult($this->getSoapClient()->initTransaction($_wPStructInitTransaction)->return);
    }

    public function getResult()
    {
        return parent::getResult();
    }

    public function __toString()
    {
        return __CLASS__;
    }
}
