<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructWpmDetailInput extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $serviceId;
    public $cardHolderId;
    public $cardHolderName;
    public $cardHolderLastName1;
    public $cardHolderLastName2;
    public $cardHolderMail;
    public $cellPhoneNumber;
    public $expirationDate;
    public $commerceMail;
    public $ufFlag;
    public function __construct($_serviceId = NULL,$_cardHolderId = NULL,$_cardHolderName = NULL,$_cardHolderLastName1 = NULL,$_cardHolderLastName2 = NULL,$_cardHolderMail = NULL,$_cellPhoneNumber = NULL,$_expirationDate = NULL,$_commerceMail = NULL,$_ufFlag = NULL)
    {
        parent::__construct(array('serviceId'=>$_serviceId,'cardHolderId'=>$_cardHolderId,'cardHolderName'=>$_cardHolderName,'cardHolderLastName1'=>$_cardHolderLastName1,'cardHolderLastName2'=>$_cardHolderLastName2,'cardHolderMail'=>$_cardHolderMail,'cellPhoneNumber'=>$_cellPhoneNumber,'expirationDate'=>$_expirationDate,'commerceMail'=>$_commerceMail,'ufFlag'=>$_ufFlag),false);
    }
    public function getServiceId()
    {
        return $this->serviceId;
    }
    public function setServiceId($_serviceId)
    {
        return ($this->serviceId = $_serviceId);
    }
    public function getCardHolderId()
    {
        return $this->cardHolderId;
    }
    public function setCardHolderId($_cardHolderId)
    {
        return ($this->cardHolderId = $_cardHolderId);
    }
    public function getCardHolderName()
    {
        return $this->cardHolderName;
    }
    public function setCardHolderName($_cardHolderName)
    {
        return ($this->cardHolderName = $_cardHolderName);
    }
    public function getCardHolderLastName1()
    {
        return $this->cardHolderLastName1;
    }
    public function setCardHolderLastName1($_cardHolderLastName1)
    {
        return ($this->cardHolderLastName1 = $_cardHolderLastName1);
    }
    public function getCardHolderLastName2()
    {
        return $this->cardHolderLastName2;
    }
    public function setCardHolderLastName2($_cardHolderLastName2)
    {
        return ($this->cardHolderLastName2 = $_cardHolderLastName2);
    }
    public function getCardHolderMail()
    {
        return $this->cardHolderMail;
    }
    public function setCardHolderMail($_cardHolderMail)
    {
        return ($this->cardHolderMail = $_cardHolderMail);
    }
    public function getCellPhoneNumber()
    {
        return $this->cellPhoneNumber;
    }
    public function setCellPhoneNumber($_cellPhoneNumber)
    {
        return ($this->cellPhoneNumber = $_cellPhoneNumber);
    }
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }
    public function setExpirationDate($_expirationDate)
    {
        return ($this->expirationDate = $_expirationDate);
    }
    public function getCommerceMail()
    {
        return $this->commerceMail;
    }
    public function setCommerceMail($_commerceMail)
    {
        return ($this->commerceMail = $_commerceMail);
    }
    public function getUfFlag()
    {
        return $this->ufFlag;
    }
    public function setUfFlag($_ufFlag)
    {
        return ($this->ufFlag = $_ufFlag);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
