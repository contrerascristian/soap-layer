<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPEnumWsTransactionType extends \Tiandgi\SoapLayer\TransbankWsdlClass
{
    const NORMAL = 'TR_NORMAL_WS';
    const PATPASS = 'TR_NORMAL_WS_WPM';
    const MALL = 'TR_MALL_WS';
    public static function ValidTransactionType($_value)
    {
        return in_array($_value,array(WPEnumWsTransactionType::NORMAL,WPEnumWsTransactionType::PATPASS,WPEnumWsTransactionType::MALL));
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
