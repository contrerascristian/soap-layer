<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructWsInitTransactionInput extends \Tiandgi\SoapLayer\BaseObject
{
    public $wSTransactionType;
    public $commerceId;
    public $buyOrder;
    public $sessionId;
    public $returnURL;
    public $finalURL;
    public $transactionDetails;
    public $wPMDetail;

    public function __construct($arrayObject)
    {
        if(!WPEnumWsTransactionType::ValidTransactionType($arrayObject["wSTransactionType"]))
        {
            throw new \Exception("No existe transaccion ".$arrayObject["wSTransactionType"]." en Modelo SOAP");
        }
        return parent::__construct($arrayObject);
    }

    public function __toString()
    {
        return __CLASS__;
    }
}
