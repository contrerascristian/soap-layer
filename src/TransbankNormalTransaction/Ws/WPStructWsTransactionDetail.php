<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructWsTransactionDetail extends \Tiandgi\SoapLayer\BaseObject
{
    public $sharesAmount;
    public $sharesNumber;
    public $amount;
    public $commerceCode;
    public $buyOrder;

    public function __construct($_sharesAmount = NULL,$_sharesNumber = NULL,$_amount = NULL,$_commerceCode = NULL,$_buyOrder = NULL)
    {
        parent::__construct(array('sharesAmount'=>$_sharesAmount,'sharesNumber'=>$_sharesNumber,'amount'=>$_amount,'commerceCode'=>$_commerceCode,'buyOrder'=>$_buyOrder));
    }

    public function __toString()
    {
        return __CLASS__;
    }
}
