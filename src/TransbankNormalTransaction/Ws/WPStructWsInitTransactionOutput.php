<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructWsInitTransactionOutput extends \Tiandgi\SoapLayer\BaseObject
{
    public $token;
    public $url;

    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
