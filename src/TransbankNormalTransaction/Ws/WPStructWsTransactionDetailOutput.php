<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructWsTransactionDetailOutput extends WPStructWsTransactionDetail
{

    public $authorizationCode;
    public $paymentTypeCode;
    public $responseCode;
    public function __construct($_authorizationCode = NULL,$_paymentTypeCode = NULL,$_responseCode = NULL)
    {
        WPWsdlClass::__construct(array('authorizationCode'=>$_authorizationCode,'paymentTypeCode'=>$_paymentTypeCode,'responseCode'=>$_responseCode),false);
    }
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }
    public function setAuthorizationCode($_authorizationCode)
    {
        return ($this->authorizationCode = $_authorizationCode);
    }
    public function getPaymentTypeCode()
    {
        return $this->paymentTypeCode;
    }
    public function setPaymentTypeCode($_paymentTypeCode)
    {
        return ($this->paymentTypeCode = $_paymentTypeCode);
    }
    public function getResponseCode()
    {
        return $this->responseCode;
    }
    public function setResponseCode($_responseCode)
    {
        return ($this->responseCode = $_responseCode);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
