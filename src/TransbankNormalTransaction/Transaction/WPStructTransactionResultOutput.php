<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructTransactionResultOutput extends \Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $accountingDate;
    public $buyOrder;
    public $cardDetail;
    public $detailOutput;
    public $sessionId;
    public $transactionDate;
    public $urlRedirection;
    public $VCI;

    public function __construct($_accountingDate = NULL,$_buyOrder = NULL,$_cardDetail = NULL,$_detailOutput = NULL,$_sessionId = NULL,$_transactionDate = NULL,$_urlRedirection = NULL,$_vCI = NULL)
    {
        parent::__construct(array('accountingDate'=>$_accountingDate,'buyOrder'=>$_buyOrder,'cardDetail'=>$_cardDetail,'detailOutput'=>$_detailOutput,'sessionId'=>$_sessionId,'transactionDate'=>$_transactionDate,'urlRedirection'=>$_urlRedirection,'VCI'=>$_vCI),false);
    }

    public function getAccountingDate()
    {
        return $this->accountingDate;
    }
    public function setAccountingDate($_accountingDate)
    {
        return ($this->accountingDate = $_accountingDate);
    }
    public function getBuyOrder()
    {
        return $this->buyOrder;
    }
    public function setBuyOrder($_buyOrder)
    {
        return ($this->buyOrder = $_buyOrder);
    }
    public function getCardDetail()
    {
        return $this->cardDetail;
    }
    public function setCardDetail($_cardDetail)
    {
        return ($this->cardDetail = $_cardDetail);
    }
    public function getDetailOutput()
    {
        return $this->detailOutput;
    }
    public function setDetailOutput($_detailOutput)
    {
        return ($this->detailOutput = $_detailOutput);
    }
    public function getSessionId()
    {
        return $this->sessionId;
    }
    public function setSessionId($_sessionId)
    {
        return ($this->sessionId = $_sessionId);
    }
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }
    public function setTransactionDate($_transactionDate)
    {
        return ($this->transactionDate = $_transactionDate);
    }
    public function getUrlRedirection()
    {
        return $this->urlRedirection;
    }
    public function setUrlRedirection($_urlRedirection)
    {
        return ($this->urlRedirection = $_urlRedirection);
    }
    public function getVCI()
    {
        return $this->VCI;
    }
    public function setVCI($_vCI)
    {
        return ($this->VCI = $_vCI);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
