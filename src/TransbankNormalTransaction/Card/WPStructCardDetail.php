<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPStructCardDetail extends Tiandgi\SoapLayer\TransbankWsdlClass
{

    public $cardNumber;

    public $cardExpirationDate;

    public function __construct($_cardNumber = NULL,$_cardExpirationDate = NULL)
    {
        parent::__construct(array('cardNumber'=>$_cardNumber,'cardExpirationDate'=>$_cardExpirationDate),false);
    }

    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    public function setCardNumber($_cardNumber)
    {
        return ($this->cardNumber = $_cardNumber);
    }

    public function getCardExpirationDate()
    {
        return $this->cardExpirationDate;
    }

    public function setCardExpirationDate($_cardExpirationDate)
    {
        return ($this->cardExpirationDate = $_cardExpirationDate);
    }

    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }

    public function __toString()
    {
        return __CLASS__;
    }
}
