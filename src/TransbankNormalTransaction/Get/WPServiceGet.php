<?php

namespace Tiandgi\SoapLayer\TransbankNormalTransaction;

class WPServiceGet extends Tiandgi\SoapLayer\TransbankWsdlClass
{

    public function getTransactionResult(WPStructGetTransactionResult $_wPStructGetTransactionResult)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->getTransactionResult($_wPStructGetTransactionResult));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }

    public function getResult()
    {
        return parent::getResult();
    }

    public function __toString()
    {
        return __CLASS__;
    }
}
