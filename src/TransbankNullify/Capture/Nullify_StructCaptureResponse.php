<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_StructCaptureResponse extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $return;
    public function __construct($_return = NULL)
    {
        parent::__construct(array('return'=>$_return),false);
    }
    public function getReturn()
    {
        return $this->return;
    }
    public function setReturn($_return)
    {
        return ($this->return = $_return);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
