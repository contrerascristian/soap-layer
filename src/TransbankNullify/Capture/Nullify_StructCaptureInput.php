<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_StructCaptureInput extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $commerceId;
    public $buyOrder;
    public $authorizationCode;
    public $captureAmount;
    public function __construct($_commerceId = NULL,$_buyOrder = NULL,$_authorizationCode = NULL,$_captureAmount = NULL)
    {
        parent::__construct(array('commerceId'=>$_commerceId,'buyOrder'=>$_buyOrder,'authorizationCode'=>$_authorizationCode,'captureAmount'=>$_captureAmount),false);
    }
    public function getCommerceId()
    {
        return $this->commerceId;
    }
    public function setCommerceId($_commerceId)
    {
        return ($this->commerceId = $_commerceId);
    }
    public function getBuyOrder()
    {
        return $this->buyOrder;
    }
    public function setBuyOrder($_buyOrder)
    {
        return ($this->buyOrder = $_buyOrder);
    }
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }
    public function setAuthorizationCode($_authorizationCode)
    {
        return ($this->authorizationCode = $_authorizationCode);
    }
    public function getCaptureAmount()
    {
        return $this->captureAmount;
    }
    public function setCaptureAmount($_captureAmount)
    {
        return ($this->captureAmount = $_captureAmount);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
