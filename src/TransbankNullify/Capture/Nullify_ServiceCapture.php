<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_ServiceCapture extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public function capture(Nullify_StructCapture $_nullify_StructCapture)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->capture($_nullify_StructCapture));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    public function getResult()
    {
        return parent::getResult();
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
