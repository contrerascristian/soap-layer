<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_StructCapture extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $captureInput;
    public function __construct($_captureInput = NULL)
    {
        parent::__construct(array('captureInput'=>$_captureInput),false);
    }
    public function getCaptureInput()
    {
        return $this->captureInput;
    }
    public function setCaptureInput($_captureInput)
    {
        return ($this->captureInput = $_captureInput);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
