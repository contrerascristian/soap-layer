<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_StructCaptureOutput extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $authorizationCode;
    public $authorizationDate;
    public $capturedAmount;
    public $token;
    public function __construct($_authorizationCode = NULL,$_authorizationDate = NULL,$_capturedAmount = NULL,$_token = NULL)
    {
        parent::__construct(array('authorizationCode'=>$_authorizationCode,'authorizationDate'=>$_authorizationDate,'capturedAmount'=>$_capturedAmount,'token'=>$_token),false);
    }
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }
    public function setAuthorizationCode($_authorizationCode)
    {
        return ($this->authorizationCode = $_authorizationCode);
    }
    public function getAuthorizationDate()
    {
        return $this->authorizationDate;
    }
    public function setAuthorizationDate($_authorizationDate)
    {
        return ($this->authorizationDate = $_authorizationDate);
    }
    public function getCapturedAmount()
    {
        return $this->capturedAmount;
    }
    public function setCapturedAmount($_capturedAmount)
    {
        return ($this->capturedAmount = $_capturedAmount);
    }
    public function getToken()
    {
        return $this->token;
    }
    public function setToken($_token)
    {
        return ($this->token = $_token);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
