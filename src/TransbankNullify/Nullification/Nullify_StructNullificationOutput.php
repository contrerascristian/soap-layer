<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_StructNullificationOutput extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $authorizationCode;
    public $authorizationDate;
    public $balance;
    public $nullifiedAmount;
    public $token;
    public function __construct($_authorizationCode = NULL,$_authorizationDate = NULL,$_balance = NULL,$_nullifiedAmount = NULL,$_token = NULL)
    {
        Nullify_WsdlClass::__construct(array('authorizationCode'=>$_authorizationCode,'authorizationDate'=>$_authorizationDate,'balance'=>$_balance,'nullifiedAmount'=>$_nullifiedAmount,'token'=>$_token),false);
    }
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }
    public function setAuthorizationCode($_authorizationCode)
    {
        return ($this->authorizationCode = $_authorizationCode);
    }
    public function getAuthorizationDate()
    {
        return $this->authorizationDate;
    }
    public function setAuthorizationDate($_authorizationDate)
    {
        return ($this->authorizationDate = $_authorizationDate);
    }
    public function getBalance()
    {
        return $this->balance;
    }
    public function setBalance($_balance)
    {
        return ($this->balance = $_balance);
    }
    public function getNullifiedAmount()
    {
        return $this->nullifiedAmount;
    }
    public function setNullifiedAmount($_nullifiedAmount)
    {
        return ($this->nullifiedAmount = $_nullifiedAmount);
    }
    public function getToken()
    {
        return $this->token;
    }
    public function setToken($_token)
    {
        return ($this->token = $_token);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
