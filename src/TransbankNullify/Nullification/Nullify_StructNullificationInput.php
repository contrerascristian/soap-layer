<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_StructNullificationInput extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $commerceId;
    public $buyOrder;
    public $authorizedAmount;
    public $authorizationCode;
    public $nullifyAmount;
    public function __construct($_commerceId = NULL,$_buyOrder = NULL,$_authorizedAmount = NULL,$_authorizationCode = NULL,$_nullifyAmount = NULL)
    {
        Nullify_WsdlClass::__construct(array('commerceId'=>$_commerceId,'buyOrder'=>$_buyOrder,'authorizedAmount'=>$_authorizedAmount,'authorizationCode'=>$_authorizationCode,'nullifyAmount'=>$_nullifyAmount),false);
    }
    public function getCommerceId()
    {
        return $this->commerceId;
    }
    public function setCommerceId($_commerceId)
    {
        return ($this->commerceId = $_commerceId);
    }
    public function getBuyOrder()
    {
        return $this->buyOrder;
    }
    public function setBuyOrder($_buyOrder)
    {
        return ($this->buyOrder = $_buyOrder);
    }
    public function getAuthorizedAmount()
    {
        return $this->authorizedAmount;
    }
    public function setAuthorizedAmount($_authorizedAmount)
    {
        return ($this->authorizedAmount = $_authorizedAmount);
    }
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }
    public function setAuthorizationCode($_authorizationCode)
    {
        return ($this->authorizationCode = $_authorizationCode);
    }
    public function getNullifyAmount()
    {
        return $this->nullifyAmount;
    }
    public function setNullifyAmount($_nullifyAmount)
    {
        return ($this->nullifyAmount = $_nullifyAmount);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
