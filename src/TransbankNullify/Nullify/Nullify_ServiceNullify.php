<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_ServiceNullify extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public function nullify(Nullify_StructNullify $_nullify_StructNullify)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->nullify($_nullify_StructNullify));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    public function getResult()
    {
        return parent::getResult();
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
