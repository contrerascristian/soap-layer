<?php

namespace Tiandgi\SoapLayer\Nullify;

class Nullify_StructNullify extends Tiandgi\SoapLayer\TransbankWsdlClass
{
    public $nullificationInput;
    public function __construct($_nullificationInput = NULL)
    {
        parent::__construct(array('nullificationInput'=>$_nullificationInput),false);
    }
    public function getNullificationInput()
    {
        return $this->nullificationInput;
    }
    public function setNullificationInput($_nullificationInput)
    {
        return ($this->nullificationInput = $_nullificationInput);
    }
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    public function __toString()
    {
        return __CLASS__;
    }
}
