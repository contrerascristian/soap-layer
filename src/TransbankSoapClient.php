<?php
namespace Tiandgi\SoapLayer;

use \Tiandgi\SoapLayer\External as External;


class Transbank_SoapClient extends \SoapClient {

    public $useSSL = false;
    public $privateKey = "";
    public $publicCert = "";
    public $webpayCert = "";

    function __construct($wsdl, $privateKey, $publicCert, $webpayCert, $options) {

        $locationparts = parse_url($wsdl);
        $this->useSSL = $locationparts['scheme'] == "https" ? true : false;
        $this->privateKey = $privateKey;
        $this->publicCert = $publicCert;
        $this->webpayCert = $webpayCert;
        return parent::__construct($wsdl, $options);
    }

    function __doRequest($request, $location, $saction, $version, $one_way = 0) {


        $doc = new \DOMDocument('1.0');

        $doc->loadXML($request);
        $objWSSE = new External\WSSESoap($doc);
        $objKey = new External\XMLSecurityKey(External\XMLSecurityKey::RSA_SHA1, array(
            'type' => 'private'
        ));
        $objKey->loadKey($this->privateKey, true);
        $options = array(
            "insertBefore" => TRUE
        );

        $objWSSE->signSoapDoc($objKey, $options);
        $objWSSE->addIssuerSerial($this->publicCert);


        $retVal = parent::__doRequest($objWSSE->saveXML(), $location, $saction, $version);

        $doc = new \DOMDocument();
        $doc->loadXML($retVal);
        return $this->validateResponse($doc->saveXML());

    }

    private function validateResponse($xmlResponse){
                $soapValidation = new External\SoapValidation($xmlResponse,$this->webpayCert);
                if($soapValidation->getValidationResult() === true)
                    return $xmlResponse;
                else
                    throw new Exception("Error validando conexión a Webpay (Verificar que la información del certificado sea correcta)");

    }

}

?>
